<?php

namespace App\Controllers;

use App\Models\UserEventModel;

class UserEvents extends Controller {
    protected $userEventModel;

    public function __construct($params) {
        $this->userEventModel = new UserEventModel();
        parent::__construct($params);
    }

    public function getUserEvents() {
        $userId = $this->params['organisateur_id'] ?? null;

        if (!$userId) {
            return [
                'code' => '400',
                'message' => 'User ID is required.'
            ];
        }

        $events = $this->userEventModel->getEventsByUserId($userId);

        if ($events) {
            return [
                'code' => '200',
                'events' => $events
            ];
        } else {
            return [
                'code' => '404',
                'message' => 'No events found for this user.'
            ];
        }
    }
}
