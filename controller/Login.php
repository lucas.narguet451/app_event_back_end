<?php

// Register 

namespace App\Controllers;

use App\Models\LoginModel;

class Login extends Controller {
    protected object $login;

    public function __construct($param) {
        $this->login = new LoginModel();
        parent::__construct($param);
    }

    public function postlogin() {
        $this->login->add($this->body);
        return $this->login->getLast();
    }

    public function deletelogin() {
        return $this->login->delete(intval($this->params['id']));
    }

    public function getlogin() {
        return $this->login->getAll();
    }
}

?>
