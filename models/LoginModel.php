<?php

namespace App\Models;

use PDO;
use stdClass;

class LoginModel extends SqlConnect {
    public function add(array $data) {
        $data['mot_de_passe'] = password_hash($data['mot_de_passe'], PASSWORD_DEFAULT);

        $query = "
        INSERT INTO users (nom, prenom, email, mot_de_passe, role)
        VALUES (:nom, :prenom, :email, :mot_de_passe, :role)
        ";

        $req = $this->db->prepare($query);
        $req->execute($data);
    }

    public function delete(int $id) {
        $req = $this->db->prepare("DELETE FROM users WHERE id = :id");
        $req->execute(["id" => $id]);
    }

    public function get(int $id) {
        $req = $this->db->prepare("SELECT * FROM users WHERE id = :id");
        $req->execute(["id" => $id]);

        return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }

    public function getAll() {
        $req = $this->db->prepare("SELECT * FROM users");
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getLast() {
        $req = $this->db->prepare("SELECT * FROM users ORDER BY id DESC LIMIT 1");
        $req->execute();

        return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }

    public function checkLogin(string $email, string $password) {
        $req = $this->db->prepare("SELECT * FROM users WHERE email = :email");
        $req->execute(["email" => $email]);
    
        $user = $req->fetch(PDO::FETCH_ASSOC);
    
        // Affiche la structure de $user pour le débogage
        var_dump($user);
    
        if ($user && isset($user['mot_de_passe']) && is_string($user['mot_de_passe']) && password_verify($password, $user['mot_de_passe'])) {
            return $user;
        } else {
            return false;
        }
    }
    
}
?>
