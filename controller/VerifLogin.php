<?php

namespace App\Controllers;

use App\Models\LoginModel;

class VerifLogin extends Controller {
    protected object $login;

    public function __construct($param) {
        $this->login = new LoginModel();
        parent::__construct($param);
    }

    // Méthode pour POST requests
    public function postVerifLogin() {
        // Affiche les données POST pour le débogage
        var_dump($_POST);

        // Vérifiez si le contenu est de type JSON et décodez-le
        $input = file_get_contents('php://input');
        $data = json_decode($input, true);

        if (json_last_error() === JSON_ERROR_NONE) {
            $email = $data['email'] ?? '';
            $password = $data['mot_de_passe'] ?? '';
        } else {
            $email = $_POST['email'] ?? '';
            $password = $_POST['mot_de_passe'] ?? '';
        }

        // Affiche les valeurs récupérées
        var_dump($email, $password);

        if (empty($email) || empty($password)) {
            return [
                'code' => '400',
                'message' => 'Email and password are required.'
            ];
        }

        $user = $this->login->checkLogin($email, $password);

        if ($user) {
            session_start();
            $session_id = session_id();
            return [
                'code' => '200',
                'message' => 'Login successful',
                'user' => $user,
                'session_id' => $session_id
            ];
        } else {
            return [
                'code' => '401',
                'message' => 'Invalid email or password.'
            ];
        }
    }

    // Méthode pour GET requests
    public function getVerifLogin() {
        return $this->login->getAll();
    }
}
?>
