<?php

namespace App\Controllers;

class Controller {
    protected array $params;
    protected string $reqMethod;
    protected array $body;
    protected string $className;

    public function __construct($params) {
        $this->className = strtolower($this->getCallerClassName());
        $this->params = $params;
        $this->reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
        $this->body = (array) json_decode(file_get_contents('php://input'), true);

        $this->header();
        if ($this->reqMethod === 'options') {
            $this->handlePreflight();
            return;
        }
        $this->ifMethodExist();
    }

    protected function getCallerClassName() {
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 2);
        if (isset($backtrace[1]['object'])) {
            $fullClassName = get_class($backtrace[1]['object']);
            $className = basename(str_replace('\\', '/', $fullClassName));
            return $className;
        }
        return 'Unknown';
    }

    protected function header() {
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json; charset=utf-8');
        header('Access-Control-Allow-Methods: GET, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Content-Type, Authorization');
    }

    protected function handlePreflight() {
        header('HTTP/1.1 204 No Content');
        exit;
    }

    protected function ifMethodExist() {
        $method = $this->reqMethod . $this->className;
        error_log("Checking method: $method"); // Log the method being checked
        if (method_exists($this, $method)) {
            echo json_encode($this->$method());
            return;
        }

        header('HTTP/1.0 404 Not Found');
        echo json_encode([
            'code' => '404',
            'message' => 'Not Found'
        ]);
    }
}

?>
