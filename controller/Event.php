<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\EventModel;

class Event extends Controller {
    protected object $event;

    public function __construct($param) {
        $this->event = new EventModel();
        parent::__construct($param); 
    }

    public function postEvent() {
        try {
            $this->event->add($this->body);
            http_response_code(201); // Created
            return $this->event->getLast();
        } catch (\Exception $e) {
            http_response_code(500); // Internal Server Error
            return ['error' => $e->getMessage()];
        }
    }

    public function deleteEvent() {
        try {
            if (!isset($this->params['id'])) {
                throw new \Exception("Missing 'id' parameter");
            }
            $this->event->delete(intval($this->params['id']));
            http_response_code(204); // No Content
            return [];
        } catch (\Exception $e) {
            http_response_code(400); // Bad Request
            return ['error' => $e->getMessage()];
        }
    }
}
