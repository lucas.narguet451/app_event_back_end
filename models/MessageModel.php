<?php

namespace App\Models;

use PDO;
use PDOException;
use stdClass;

class MessageModel extends SqlConnect {
    public function add(array $data) {
        try {
            $query = "
                INSERT INTO message (name, text_messages, date)
                VALUES (:firstname, :name, :text_messages, :date)
            ";

            $req = $this->db->prepare($query);
            $req->execute($data);
        } catch (PDOException $e) {
            
            echo 'Erreur PDO : ' . $e->getMessage();
            exit;
        }
    }

    public function delete(int $id) {
        try {
            $req = $this->db->prepare("DELETE FROM message WHERE id = :id");
            $req->execute(["id" => $id]);
        } catch (PDOException $e) {
            
            echo 'Erreur PDO : ' . $e->getMessage();
            exit;
        }
    }

    public function get(int $id) {
        try {
            $req = $this->db->prepare("SELECT * FROM message WHERE id = :id");
            $req->execute(["id" => $id]);
            return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
        } catch (PDOException $e) {
            
            echo 'Erreur PDO : ' . $e->getMessage();
            exit;
        }
    }

    public function getLast() {
        try {
            $req = $this->db->prepare("SELECT * FROM message ORDER BY id DESC LIMIT 1");
            $req->execute();
            return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
        } catch (PDOException $e) {
            
            echo 'Erreur PDO : ' . $e->getMessage();
            exit;
        }
    }

    public function getAllMessages() {
        try {
            $query = "SELECT * FROM message";
            $statement = $this->db->query($query);
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            
            echo 'Erreur PDO : ' . $e->getMessage();
            exit;
        }
    }
}
