<?php

namespace App\Controllers;

use App\Models\ParticipantsModel;

class Participants {
    protected array $params;
    protected string $reqMethod;
    protected object $model;

    public function __construct($params) {
        $this->params = $params;
        $this->reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
        $this->model = new ParticipantsModel();

        $this->run();
    }

    public function getEvent() {
        $eventId = $this->params['id'] ?? null;
        if ($eventId) {
            return $this->model->get((int) $eventId);
        } else {
            return $this->model->getAllEvents();
        }
    }

    public function postEvent() {
        $data = json_decode(file_get_contents('php://input'), true);
        if ($data) {
            $this->model->add($data);
            return ['message' => 'Événement ajouté avec succès.'];
        } else {
            return ['message' => 'Données invalides.', 'code' => 400];
        }
    }

    public function deleteEvent() {
        $eventId = $this->params['id'] ?? null;
        if ($eventId) {
            $this->model->delete((int) $eventId);
            return ['message' => 'Événement supprimé avec succès.'];
        } else {
            return ['message' => 'ID d\'événement manquant.', 'code' => 400];
        }
    }

    protected function header() {
        header('Access-Control-Allow-Origin: *');
        header('Content-type: application/json; charset=utf-8');
    }

    protected function ifMethodExist() {
        $method = $this->reqMethod.'Event';

        if (method_exists($this, $method)) {
            echo json_encode($this->$method());
            return;
        }

        header('HTTP/1.0 404 Not Found');
        echo json_encode([
            'code' => '404',
            'message' => 'Not found'
        ]);
    }

    protected function run() {
        $this->header();
        $this->ifMethodExist();
    }
}
