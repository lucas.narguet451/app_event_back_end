<?php

namespace App\Models;

use PDO;
use stdClass;

class EventModel extends SqlConnect {
    public function add(array $data) {
        $query = "
            INSERT INTO events (nom, description, date_debut, date_fin, lieu, nombre_inviter, budget)
            VALUES (:nom, :description, :date_debut, :date_fin, :lieu, :nombre_inviter, :budget)
        ";

        $req = $this->db->prepare($query);
        $req->execute($data);

        $results = $req->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    public function delete(int $id): bool {
        $req = $this->db->prepare("DELETE FROM events WHERE event_id = :id");
        return $req->execute(['id' => $id]);
    }

    public function get(string $name): array {
        $req = $this->db->prepare("SELECT * FROM events WHERE nom = :name");
        $req->execute(['name' => $name]);
    
        return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : [];
    }

    public function getAll() {
        $req = $this->db->prepare("SELECT * FROM events");
        $req->execute();
        $results = $req->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    public function getLast(): array {
        $req = $this->db->prepare("SELECT * FROM events ORDER BY event_id DESC LIMIT 1");
        $req->execute();

        return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }
}
