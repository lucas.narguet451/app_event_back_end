<?php

namespace App\Models;

use PDO;
use PDOException;
use stdClass;

class ParticipantsModel extends SqlConnect {
    public function add(array $data) {
        try {
            $query = "
                INSERT INTO Events (participant_id, even_id, user_id, date_inscription)
                VALUES (:participant_id, :even_id, :user_id, :date_inscription)
            ";

            $req = $this->db->prepare($query);
            $req->execute($data);
        } catch (PDOException $e) {
            echo 'Erreur PDO : ' . $e->getMessage();
            exit;
        }
    }

    public function delete(int $id) {
        try {
            $req = $this->db->prepare("DELETE FROM Events WHERE event_id = :id");
            $req->execute(["id" => $id]);
        } catch (PDOException $e) {
            echo 'Erreur PDO : ' . $e->getMessage();
            exit;
        }
    }

    public function get(int $id) {
        try {
            $req = $this->db->prepare("SELECT * FROM Events WHERE participant_id = :id");
            $req->execute(["id" => $id]);
            return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
        } catch (PDOException $e) {
            echo 'Erreur PDO : ' . $e->getMessage();
            exit;
        }
    }

    public function getLast() {
        try {
            $req = $this->db->prepare("SELECT * FROM Events ORDER BY participant_id DESC LIMIT 1");
            $req->execute();
            return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
        } catch (PDOException $e) {
            echo 'Erreur PDO : ' . $e->getMessage();
            exit;
        }
    }

    public function getAllEvents() {
        try {
            $query = "SELECT * FROM participants";
            $statement = $this->db->query($query);
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo 'Erreur PDO : ' . $e->getMessage();
            exit;
        }
    }
}
