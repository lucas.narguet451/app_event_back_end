<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\EventModel;

class Events extends Controller {
  protected object $event;

  public function __construct($param) {
    $this->event = new EventModel();
    parent::__construct($param); 
      
  }

  public function getEvents() {
    return $this->event->getAll();
   }
}