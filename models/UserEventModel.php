<?php

namespace App\Models;

use PDO;
use stdClass;

class UserEventModel extends SqlConnect {
    public function getEventsByUserId($userId) {
        $query = "SELECT * FROM events WHERE organisateur_id = :organisateur_id";
        $req = $this->db->prepare($query);
        $req->execute(['organisateur_id' => $userId]);

        return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : [];
    }
}
