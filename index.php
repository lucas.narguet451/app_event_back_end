<?php

require 'vendor/autoload.php';

use App\Router;
use App\Controllers\Message;
use App\Controllers\Participants;
use App\Controllers\Login;
use App\Controllers\VerifLogin;
use App\Controllers\Event;
use App\Controllers\Events;
use App\Controllers\UserEvents; // Assurez-vous d'avoir cette ligne

new Router([ 
  'message' => Message::class,
  'participants' => Participants::class,
  'login' => Login::class,
  'verifs' => VerifLogin::class,
  'event' => Event::class,
  'events' => Events::class,
  'user-events/:organisateur_id' => UserEvents::class // Ajoutez la nouvelle route pour les événements utilisateur
]);
